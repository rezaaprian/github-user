# Github User

Simple endless scrolling recycler view implemented traditionally without paging library.

# Feature
- Kotlin
- MVP Architecture (Interface based View)
- DiffUtils with ListAdapter to optimize recycler view update

The endless scrolling done manually via recyclerview on scrolling listener, it will fetch next batch when the list is scrolled to the bottom.

# APK

[Download apk here](https://aprionstudio.com/private/rezaaprian_manual_githubuser.apk)

# Planned updates

- MVVM Architecture (one way dependency)
- Paging Library with Remote Mediator
- NetworkBoundResource and Offline Caching