package com.aprionstudio.githubuser

import com.aprionstudio.githubuser.model.User
import com.hannesdorfmann.mosby3.mvp.MvpView

interface MainActivityView : MvpView {
    fun showLoading()
    fun hideLoading()
    fun setError(errorMessage: String)
    fun setUserRecyclerView(users: MutableList<User>, page: Int)
}