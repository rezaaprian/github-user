package com.aprionstudio.githubuser

import com.aprionstudio.githubuser.data.RemoteDataSource
import com.aprionstudio.githubuser.model.SearcUserResponse
import com.aprionstudio.githubuser.model.User
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import retrofit2.Response

const val PER_PAGE = 20

class MainActivityPresenter : MvpBasePresenter<MainActivityView>() {

    private var currentPage: Int = 0
    private var currentQuery: String = ""

    fun searchUsers(query: String, page: Int) {
        ifViewAttached { view -> view.showLoading() }
        this.currentPage = page
        this.currentQuery = query
        RemoteDataSource.getInstance().getUserByName(query, page, PER_PAGE, object :
            RemoteDataSource.GetUserByNameCallback {
            override fun onSuccess(users: MutableList<User>) {
                ifViewAttached { view ->
                    run {
                        view.setUserRecyclerView(users, page)
                        view.hideLoading()
                    }
                }
            }

            override fun onFailed(response: Response<SearcUserResponse>?, t: Throwable?) {
                ifViewAttached { view ->
                    run {
                        view.hideLoading()
                        view.setError(response?.message() ?: t.toString())
                    }
                }
            }
        })
    }

    fun fetchNext() {
        searchUsers(currentQuery, ++currentPage)
    }
}