package com.aprionstudio.githubuser.model

data class SearcUserResponse(
    var total_count: Int,
    var incomplete_results: String,
    var items: MutableList<User>
)