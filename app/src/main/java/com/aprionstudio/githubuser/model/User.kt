package com.aprionstudio.githubuser.model

data class User(
    var id: Int,
    var login: String,
    var avatar_url: String
)