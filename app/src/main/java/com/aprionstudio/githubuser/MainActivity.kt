package com.aprionstudio.githubuser

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import com.aprionstudio.githubuser.data.adapter.UserAdapter
import com.aprionstudio.githubuser.databinding.ActivityMainBinding
import com.aprionstudio.githubuser.model.User
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import androidx.recyclerview.widget.RecyclerView
import com.aprionstudio.githubuser.helpers.LinearLayoutManagerWrapper


class MainActivity : MvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: UserAdapter
    protected var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvUser.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!recyclerView.canScrollVertically(1) && dy > 0) {
                    presenter.fetchNext()
                }
            }
        })

        binding.btnSearch.setOnClickListener {
            if (binding.edtSearch.text.toString().trim().isNotEmpty()) {
                presenter.searchUsers(binding.edtSearch.text.toString().trim(), 1)
            } else {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.empty_search),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun createPresenter(): MainActivityPresenter {
        return MainActivityPresenter()
    }

    override fun showLoading() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(this)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } else {
            progressDialog!!.setMessage(getString(R.string.please_wait))
        }
        progressDialog!!.show()
    }

    override fun hideLoading() {
        if (progressDialog != null) progressDialog!!.dismiss()
    }

    override fun setError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun setUserRecyclerView(users: MutableList<User>, page: Int) {
        if (page == 1 && users.isNullOrEmpty())
            Toast.makeText(this, "User not found", Toast.LENGTH_SHORT).show()
        binding.rvUser.layoutManager = LinearLayoutManagerWrapper(this)
        if (page == 1) {
            adapter = UserAdapter(this, users)
            binding.rvUser.adapter = adapter
        } else {
            adapter.updateUsers(users)
            binding.rvUser.scrollToPosition((users.size * (page - 1)) - 1)
        }
    }
}
