package com.aprionstudio.githubuser.data

import com.aprionstudio.githubuser.data.network.APIStores
import com.aprionstudio.githubuser.data.network.NetworkClient
import com.aprionstudio.githubuser.model.SearcUserResponse
import com.aprionstudio.githubuser.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteDataSource {
    companion object {
        @Volatile
        private var instance: RemoteDataSource? = null

        @Synchronized
        fun getInstance(): RemoteDataSource = instance ?: RemoteDataSource().also { instance = it }
    }

    fun getUserByName(query: String, page: Int, per_page: Int, callback: GetUserByNameCallback) {
        val rapidAPI = NetworkClient.getRetrofit()?.create(APIStores::class.java)
        rapidAPI?.getUserByName(query, page, per_page)
            ?.enqueue(object : Callback<SearcUserResponse> {
                override fun onResponse(
                    call: Call<SearcUserResponse>,
                    response: Response<SearcUserResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (200 == response.code()) {
                            callback.onSuccess(response.body()!!.items)
                        } else {
                            callback.onFailed(response, null)
                        }
                    } else {
                        callback.onFailed(response, null)
                    }
                }

                override fun onFailure(call: Call<SearcUserResponse>, t: Throwable) {
                    callback.onFailed(null, t)
                }
            })
    }

    interface GetUserByNameCallback {
        fun onSuccess(users: MutableList<User>)
        fun onFailed(response: Response<SearcUserResponse>?, t: Throwable?)
    }
}