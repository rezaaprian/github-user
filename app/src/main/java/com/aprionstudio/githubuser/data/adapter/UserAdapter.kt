package com.aprionstudio.githubuser.data.adapter

import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aprionstudio.githubuser.databinding.UserItemBinding
import com.aprionstudio.githubuser.model.User
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop

class UserAdapter(private val activity: Activity, private var users: MutableList<User>) :
    ListAdapter<User, UserAdapter.ViewHolder>(UserDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        return ViewHolder(UserItemBinding.inflate(activity.layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        holder.bindHolder(users[position])
    }

    fun updateUsers(users: List<User>) {
        if (!users.isNullOrEmpty()) {
            this.users.addAll(users)
            submitList(this.users)
        }
    }

    override fun getItemCount(): Int = this.users.size

    inner class ViewHolder(private val binding: UserItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindHolder(user: User) {
            binding.txtUser.text = user.login
            Glide.with(itemView.context)
                .load(user.avatar_url)
                .transform(CircleCrop())
                .into(binding.imgUser)
        }
    }

    class UserDiffCallback : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }
}