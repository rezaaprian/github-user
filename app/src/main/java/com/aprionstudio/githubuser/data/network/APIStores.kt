package com.aprionstudio.githubuser.data.network

import com.aprionstudio.githubuser.model.SearcUserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIStores {
    @GET("search/users")
    fun getUserByName(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int,
    ): Call<SearcUserResponse>
}